let x_values = [];
let y_values = [];
let teta1, teta0;
const learningRate = 0.01;
const optimizer = tf.train.sgd(learningRate);

function setup() {
    createCanvas(400, 400);
    teta1 = tf.variable(tf.scalar(random(1)));
    teta0 = tf.variable(tf.scalar(random(1)));
}

function loss(pred, labels) {
    return pred.sub(labels).square().mean();
}

function predict(x) {
    const xs = tf.tensor1d(x);
    // y = mx + b;
    const ys = xs.mul(teta1).add(teta0);
    return ys;
}

function getXY() {
    let start1 = 0
    let stop1 = 400
    let start2 = 0
    let stop2 = 1
    const x = document.getElementById("x_in").value;
    x_maped = map(parseInt(x), start1, stop1, start2, stop2);
    x_values.push(x_maped);
    //console.log(x);
    const y = document.getElementById("y_in").value;
    start2 = 1
    stop2 = 0
    y_maped = map(parseInt(y), start1, stop1, start2, stop2);
    y_values.push(y_maped);
    draw();
    //console.log(y);
}

function draw() {
    console.log("HERE");
    tf.tidy(() => {
        if (x_values.length > 0) {
            const ys = tf.tensor1d(y_values);
            optimizer.minimize(() => loss(predict(x_values), ys));
        }
    });
    background(120);
    stroke(255);
    strokeWeight(8);
    for (let i = 0; i < x_values.length; i++) {
        let px = map(x_values[i], 0, 1, 0, width);
        let py = map(y_values[i], 0, 1, height, 0);
        point(px, py);
    }

    const lineX = [0, 1];

    const ys = tf.tidy(() => predict(lineX));
    let lineY = ys.dataSync();
    ys.dispose();

    let x1 = map(lineX[0], 0, 1, 0, width);
    let x2 = map(lineX[1], 0, 1, 0, width);
    let y1 = map(lineY[0], 0, 1, height, 0);
    let y2 = map(lineY[1], 0, 1, height, 0);

    strokeWeight(2);
    line(x1, y1, x2, y2);

    console.log(tf.memory().numTensors);
}