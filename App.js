let x_values = [];
let y_values = [];
let x_unmaped = [];
let y_unmaped = [];
let teta1, teta0;
const learningRate = 0.25;
let width = 800;
let height = 800;
let epochs = 0;

//const optimizer = tf.train.sgd(learningRate);

function setup() {
    //console.log("1");
    tabularCreator();
    let box = document.querySelector("#canvas");
    width = box.clientWidth+20;
    height = window.innerHeight/2
    console.log(width)
    /*const canvas = createCanvas(width, width);
    canvas.parent('canvas');*/
    teta1 = tf.variable(tf.scalar(random(1)));
    teta0 = tf.variable(tf.scalar(random(1)));
    grad = document.querySelector("#gradient");
    grad.innerText = teta1.dataSync();
    inter = document.querySelector("#intercept");
    inter.innerText = teta0.dataSync();
}

function show_params() {
    var table = document.getElementById("table");
    if (table.hasChildNodes()) {
        table.removeChild(table.getElementsByTagName("tbody")[0]);
    }
    let tbody = table.createTBody();
    for (let i = 0; i < x_values.length; i++) {
        var row = tbody.insertRow();
        const x = x_unmaped[i];
        const y = y_unmaped[i];
        var col0 = row.insertCell(0);
        //const num = x_values.length
        col0.innerHTML = i;
        var col1 = row.insertCell(1);
        col1.innerHTML = x;
        var col2 = row.insertCell(2);
        col2.innerHTML = y;
        var erasebtn = row.insertCell(3);
        erasebtn.innerHTML = "<button onclick='delXY(" + i.toString() + ")'" + " class='btn btn-danger btn-sm' title='Delete' data-toggle='tooltip' data-placement='top'><i class='fa fa-trash'></i></button>"
        //tbody.innerHTML = row;
    }
    // let row = document.createElement("TR");
    // var row = tbody.insertRow();
    // var col0 = row.insertCell(0);
    // const num = x_values.length
    // col0.innerHTML = num;
    // var col1 = row.insertCell(1);
    // col1.innerHTML = x;
    // var col2 = row.insertCell(2);
    // col2.innerHTML = y;
    // var erasebtn = row.insertCell(3);
    // erasebtn.innerHTML = "<button onclick='delXY(" + num.toString() + ")'" + " class='btn btn-danger btn-sm' title='Delete' data-toggle='tooltip' data-placement='top'><i class='fa fa-trash'></i></button>"

}

function delXY(index) {
    //console.log("OK");
    x_unmaped.splice(index, index + 1);
    y_unmaped.splice(index, index + 1);
    x_values.splice(index, index + 1);
    y_values.splice(index, index + 1);
    // console.log(y_values);
    show_params();
}

function loss(pred, labels) {
    return pred.sub(labels).square().mean();
}

function predict(x) {
    const xs = tf.tensor1d(x);
    // y = mx + b;
    const ys = xs.mul(teta1).add(teta0);
    return ys;
}

function getSingleXY() {
    let start1 = 0;
    let start2 = 0;
    let stop2 = 1;
    const x = document.getElementById("x_in").value;
    const y_str = document.getElementById("y_in").value;
    const numbers = /^[-+]?[0-9]+$/;
    if (x.match(numbers) && y_str.match(numbers)) {
        x_unmaped.push(x);
        let x_scaled = parseInt(x);
        //console.log(width);
        if (x_scaled < (width / 2)) x_scaled += 5;
        else x_scaled -= 5;
        //console.log(x_scaled);
        x_maped = map(x_scaled, start1, width, start2, stop2);
        x_values.push(x_maped);
        y_unmaped.push(y_str);
        let y = parseInt(y_str);
        if (y < (width / 2)) y += 5;
        else y -= 5;
        //console.log(y);
        let y_scaled = ((height / 2) - y) * 2 + y;
        start2 = 1;
        stop2 = 0;
        y_maped = map(y_scaled, start1, height, start2, stop2);
        y_values.push(y_maped);
        document.getElementById("x_in").value = '';
        document.getElementById("y_in").value = '';
        epochs = 0;
        show_params();
        draw();
    }
}

function getMulXY() {
    let start1 = 0;
    let start2 = 0;
    let stop2 = 1;
    let ta = document.getElementById("xy_ta").value;
    const textarea = ta.split("\n");
    document.getElementById("xy_ta").value = '';
    const numbers = /^[-+]?[0-9]+$/;
    textarea.forEach(element => {
        let elm_splited = element.split(",");
        let x = parseInt(elm_splited[0]);
        let y_str = parseInt(elm_splited[1]);
        if (isNaN(x) === false && isNaN(y_str) === false && elm_splited[0].match(numbers) && elm_splited[1].match(numbers)) {
            // console.log(x + " " + y_str);
            x_unmaped.push(x);
            y_unmaped.push(y_str);
            let x_scaled = parseInt(x);
            const y = parseInt(y_str);
            let y_scaled = ((height / 2) - y) * 2 + y;
            x_maped = map(x_scaled, start1, width, start2, stop2);
            x_values.push(x_maped);
            start2 = 1;
            stop2 = 0;
            y_maped = map(y_scaled, start1, height, start2, stop2);
            y_values.push(y_maped);
        }

    });
    epochs = 0;
    show_params();
    draw();

}

function tabularCreator() {
    let tb = document.querySelector("#tabular");
    jQuery('#tabular').tabularInput({
        'rows': 3,
        'columns': 2,
        'animate': true,
        'columnHeads': ['X', 'Y'],
        'name': 'myField'
    });
}

function getTabularXY() {
    const rowCounts = jQuery('#tabular')[0].childNodes[0].childNodes[1].childElementCount;
    const numbers = /^[-+]?[0-9]+$/;
    let start1 = 0;
    let start2 = 0;
    let stop2 = 1;
    for (let i = 0; i <= rowCounts; i++) {
        x_txt = "myField[0][" + i + "]"
        let x = parseInt(jQuery('[name="' + String(x_txt) + '"]').val());
        y_txt = "myField[1][" + i + "]"
        let y_str = parseInt(jQuery('[name="' + String(y_txt) + '"]').val());
        //console.log(x, y_str);
        if (isNaN(x) === false && isNaN(y_str) === false && String(x).match(numbers) && String(y_str).match(numbers)) {
            // console.log(x + " " + y_str);
            x_unmaped.push(x);
            y_unmaped.push(y_str);
            let x_scaled = parseInt(x);
            const y = parseInt(y_str);
            let y_scaled = ((height / 2) - y) * 2 + y;
            x_maped = map(x_scaled, start1, width, start2, stop2);
            x_values.push(x_maped);
            start2 = 1;
            stop2 = 0;
            y_maped = map(y_scaled, start1, height, start2, stop2);
            y_values.push(y_maped);
        }
    }
    epochs = 0;
    show_params();
    draw();
}


function draw() {
    const optimizer = tf.train.sgd(learningRate);
    if (epochs>1000) return;
    console.log(epochs)
    tf.tidy(() => {
        if (x_values.length > 0) {
            const ys = tf.tensor1d(y_values);
            optimizer.minimize(() => loss(predict(x_values), ys));
        }
    });

    /*    background(255);
        stroke(0);
        strokeWeight(8);*/
    points_arr = [];
    for (let i = 0; i < x_values.length; i++) {
        /*let px = map(x_values[i], 0, 1, 5, width);
        let py = map(y_values[i], 0, 1, height, 0);*/
        let px = parseInt(x_unmaped[i])
        let py = parseInt(y_unmaped[i])

        points_arr.push([px, py]);
        // point(px-2, py-2);
    }
    //console.log(points_arr)
    //console.log(width, height)
    var plot = {
        target: '#canvas',
        disableZoom: false,
        grid: true,
        width: width,
        height: height,
        xAxis: {
            label: 'x - axis',
            domain: [0, 800]
        },
        yAxis: {
            label: 'y - axis',
            domain: [0, 800]
        },
        data: [{
            points: points_arr,
            color: 'black',
            attr: {
                "stroke-width": "5"
            },
            fnType: 'points',
            graphType: 'scatter'
        }]
    }
    const lineX = [0, 1];

    const ys = tf.tidy(() => predict(lineX));
    if (x_values.length > 1) {
        epochs++;
        let lineY = ys.dataSync();
        ys.dispose();
        // console.log(lineX, lineY);

        let x1 = map(lineX[0], 0, 1, 0, width);
        let x2 = map(lineX[1], 0, 1, 0, width);
        let y1 = map(lineY[0], 0, 1, 0, height);
        let y2 = map(lineY[1], 0, 1, 0, height);

        //console.log(x1 + " " + x2 + " " + y1 + " " + y2);
        let a = (y2 - y1) / (x2 - x1);
        let b = (y1 - (a * x1));
        // console.log(width)
        //console.log(a + ' * x +' + b)
        plot.data.push({
            fn: a + ' * x + ' + b
        })
        le = document.querySelector("#lin_eq");
        le.innerText = "Y = " + a + " * X + " + b;
        grad = document.querySelector("#gradient");
        grad.innerText = a;
        inter = document.querySelector("#intercept");
        inter.innerText = b;
        //console.log(lineX);
        //console.log(lineY);
        /*        strokeWeight(2);
                line(x1, y1, x2, y2);
                console.log(width)
                line(0, 100, width, 500)

                stroke(200);
                line(0, 0, 0, width);
                line(0, width, width, width);*/

    }
    // console.log("helo")
    functionPlot(plot);
    // if (x_values.length > 1) {
    //     tf.tidy(() => {
    //         const ys = tf.tensor1d(y_values);
    //         optimizer.minimize(() => loss(predict(x_values), ys));
    //     });
    //     const lineX = [x_values[0], x_values[1]];
    //     const ys = tf.tidy(() => predict(lineX));
    //     let lineY = ys.dataSync();
    //     ys.dispose();

    //     let x1 = map(lineX[0], 0, 1, 0, width);
    //     let x2 = map(lineX[1], 0, 1, 0, width);
    //     let y1 = map(lineY[0], 0, 1, height, 0);
    //     let y2 = map(lineY[1], 0, 1, height, 0);

    //     strokeWeight(2);
    //     console.log("Line");
    //     line(x1, y1, x2, y2);
    // }
}
